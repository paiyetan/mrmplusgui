/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrmplus.enums;

/**
 *
 * @author paiyeta1
 */
public enum PeptideResultsPlotType {
    RESPONSE_LOD,
    RESPONSE_LLOQ_CV,
    RESPONSE_ULOQ_CV,
    RESPONSE_PVS_MaxDev,
    REPEAT_Lo_CV,
    REPEAT_Me_CV,
    REPEAT_Hi_CV,
    REPEAT_PVS_MaxDev
}
