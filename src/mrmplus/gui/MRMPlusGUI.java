/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mrmplus.gui;

import mrmplus.statistics.mappers.ExpIMetadataMapper;
import mrmplus.statistics.mappers.PeptideToRecordsMapper;
import ios.*;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;
import mrmplus.*;
import mrmplus.enums.PeptideResultsPlotType;
import mrmplus.statistics.PeptideQCEstimator;
import mrmplus.statistics.PeptideRepeatabilityMiniValidator;
import mrmplus.statistics.resultobjects.CoefficientOfVariation;
import mrmplus.statistics.resultobjects.LowerLimitOfQuantification;
import mrmplus.statistics.resultobjects.PartialValidationOfSpecificity;
import mrmplus.statistics.resultobjects.UpperLimitOfQuantification;
import mrmplus.statistics.resultobjects.experimentii.ExpIIConcLevelCoefficientOfVariation;
import mrmplus.statistics.resultobjects.experimentii.ExpIIMiniValidationOfRepeatabilityResult;
import mrmplus.statistics.resultobjects.experimentii.ExpIIPeptidePartialValidationOfSpecificity;
import mrmplus.statistics.resultobjects.experimentii.ExpIIPeptideValidatedLLOQ;
import mrmplus.statistics.resultobjects.experimentii.ExpIIValidationIdAndConcLevelsCoefs;
import org.apache.commons.math3.stat.descriptive.rank.Median;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import utilities.OneFileChooser;
import utilities.OutputDirChooser;

/**
 *
 * @author paiyeta1
 */
public class MRMPlusGUI extends javax.swing.JFrame {

    /**
     * Creates new form MRMPlusGUI
     * @throws FileNotFoundException
     * @throws IOException  
     */
    public MRMPlusGUI() throws FileNotFoundException, IOException {
        System.out.println("Starting...");
        System.out.println("Initializing GUI components...");
        initComponents();
        System.out.println("Initializing configurations...");
        setConfigurations();       
    }
    
    private void setConfigurations() throws FileNotFoundException, IOException {
        String configFile = "." + File.separator + "MRMPlus.config";
        if(new File(configFile).exists()){
            ConfigurationFileReader configR = new ConfigurationFileReader();
            this.config = configR.readConfig(configFile); 
        }
        // set default configs, if configFile isn't null.
        if(this.config != null){
            /*
             #General
            logRun=TRUE
            peptidesResultsOutputted=BOTH
            outputDirectory=./etc/projects/mrmplus/text
            #Experiment_01
            header=TRUE
            preprocessedFile=./etc/projects/mrmplus/data/exp_01/preprocessedResponseData.txt
            metadataFile=./etc/projects/mrmplus/data/exp_01/preprocessedResponseData.txt
            dilutionFile=./etc/projects/mrmplus/data/exp_01/dilution.txt
            peptidesMonitored=43
            noOftransitions=3
            preCurveBlanks=3
            totalBlanks=9
            replicates=3
            serialDilutions=7
            computeLOD=TRUE
            computeLLOQ=TRUE
            computeLinearity=FALSE
            computeCarryOver=FALSE
            computePartialValidationOfSpecificity=FALSE
            computeULOQ=FALSE
            #Experiment_02
            computeMiniValidationOfRepeatability=FALSE
            preprocessedRepeatabilityData=./etc/projects/mrmplus/data/exp_02/preprocessedRepeatabilityData.txt
            repeatabilityNoOftransitions=3
            repeatabilityNoOfPeptidesMonitored=43
            repeatabiliityNoOfConcentrationLevels=3
            repeatabilityOverNoOfDays=5
            repeatabilityNoOfReplicatesPerDay=3
            #repeatValidationOfLLOQ=TRUE
            #repeatPartialValidationOfSpecificity=TRUE
            #printDetailedRepeatabilityComputation=FALSE
            repeatabilityPrintFullView=TRUE
            #Visualization
            runVisualization=TRUE
             *  
             */ 
            
            jTextField1.setText(this.config.get("preprocessedFile"));//inputFile
            jTextField2.setText(this.config.get("metadataFile"));// metadata file...
            jTextField3.setText(this.config.get("dilutionFile"));// dilution file...
            jTextField4.setText(this.config.get("outputDirectory"));// output directory
            jTextField5.setText(this.config.get("peptidesMonitored"));// peptides monitored
            jTextField6.setText(this.config.get("noOftransitions"));// transitions
            jTextField7.setText(this.config.get("preCurveBlanks"));// precursor blanks
            jTextField8.setText(this.config.get("totalBlanks"));// total blanks
            jTextField9.setText(this.config.get("replicates"));// replicates
            jTextField10.setText(this.config.get("serialDilutions"));// serial dilutions...
            
            jComboBox1.setSelectedItem(this.config.get("computeLOD").toUpperCase());//LOD
            jComboBox2.setSelectedItem(this.config.get("computeLLOQ").toUpperCase());//LLoQ
            //jComboBox3.setSelectedItem(this.config.get("fitCurve").toUpperCase());//FitCurve
            jComboBox4.setSelectedItem(this.config.get("computeLinearity").toUpperCase());//Linearity
            jComboBox5.setSelectedItem(this.config.get("computeCarryOver").toUpperCase());//CarryOver
            jComboBox6.setSelectedItem(this.config.get("computePartialValidationOfSpecificity").toUpperCase());//Specificity
            jComboBox7.setSelectedItem(this.config.get("computeULOQ").toUpperCase());//ULoQ
            jComboBox8.setSelectedItem(this.config.get("peptidesResultsOutputted").toUpperCase());//OutputResultType
            jComboBox9.setSelectedItem(this.config.get("logRun").toUpperCase());//LogRun
            
            
            //repeatability values...
            jComboBox3.setSelectedItem(this.config.get("computeMiniValidationOfRepeatability").toUpperCase()); // Estimate repeatability TRUE/FALSE
            
            jTextField12.setText(this.config.get("preprocessedRepeatabilityData"));// input file for experiment II
            jTextField13.setText(this.config.get("repeatabilityNoOftransitions"));// rpt no of transitions monitored
            jTextField14.setText(this.config.get("repeatabilityNoOfPeptidesMonitored"));// rpt no of peptides monitores
            jTextField15.setText(this.config.get("repeatabiliityNoOfConcentrationLevels"));// rpt no of concentration levels  
            jTextField16.setText(this.config.get("repeatabilityOverNoOfDays"));// rpt no of Days
            jTextField17.setText(this.config.get("repeatabilityNoOfReplicatesPerDay"));// rpt no of replicates per day
            //repeatabilityPrintFullView=TRUE
            jComboBox10.setSelectedItem(this.config.get("repeatabilityPrintFullView").toUpperCase());
            
            // visualization...
            jComboBox13.setSelectedItem(this.config.get("runVisualization").toUpperCase());
        }
    }
    
    private void setUserConfigurations() {       
        config.put("preprocessedFile", jTextField1.getText());
        config.put("metadataFile", jTextField2.getText());
        config.put("dilutionFile", jTextField3.getText());
        config.put("outputDirectory", jTextField4.getText());
        config.put("peptidesMonitored", jTextField5.getText());
        config.put("noOftransitions", jTextField6.getText());
        config.put("preCurveBlanks", jTextField7.getText());
        config.put("totalBlanks", jTextField8.getText());
        config.put("replicates", jTextField9.getText());
        config.put("serialDilutions", jTextField10.getText());
        config.put("computeLOD", (String) jComboBox1.getSelectedItem());
        config.put("computeLLOQ", (String) jComboBox2.getSelectedItem());
        //config.put("fitCurve", (String) jComboBox3.getSelectedItem());
        config.put("computeLinearity", (String) jComboBox4.getSelectedItem());
        config.put("computeCarryOver", (String) jComboBox5.getSelectedItem());
        config.put("computePartialValidationOfSpecificity", (String) jComboBox6.getSelectedItem());
        config.put("computeULOQ", (String) jComboBox7.getSelectedItem());
        config.put("peptidesResultsOutputted", (String) jComboBox8.getSelectedItem());
        config.put("logRun",(String) jComboBox9.getSelectedItem());
        
        //experiment II
        /*
        #Experiment_02
        computeMiniValidationOfRepeatability=FALSE
        preprocessedRepeatabilityData=./etc/projects/mrmplus/data/exp_02/preprocessedRepeatabilityData.txt
        repeatabilityNoOftransitions=3
        repeatabilityNoOfPeptidesMonitored=43
        repeatabiliityNoOfConcentrationLevels=3
        repeatabilityOverNoOfDays=5
        repeatabilityNoOfReplicatesPerDay=3
        * 
        */
        config.put("computeMiniValidationOfRepeatability",(String) jComboBox3.getSelectedItem());
        
        config.put("preprocessedRepeatabilityData", jTextField12.getText());
        config.put("repeatabilityNoOftransitions", jTextField13.getText());
        config.put("repeatabilityNoOfPeptidesMonitored", jTextField14.getText());
        config.put("repeatabiliityNoOfConcentrationLevels", jTextField15.getText());
        config.put("repeatabilityOverNoOfDays", jTextField16.getText());
        config.put("repeatabilityNoOfReplicatesPerDay", jTextField17.getText());        
        //repeatabilityPrintFullView
        config.put("repeatabilityPrintFullView",(String) jComboBox10.getSelectedItem());
        
        // visualization...
        config.put("runVisualization", (String) jComboBox13.getSelectedItem());
        //jComboBox13.setSelectedItem(this.config.get("runVisualization").toUpperCase());
    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton5 = new javax.swing.JButton();
        jTextField11 = new javax.swing.JTextField();
        jButton7 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jButton11 = new javax.swing.JButton();
        jDialog1 = new javax.swing.JDialog();
        jPanel7 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jComboBox2 = new javax.swing.JComboBox();
        jComboBox4 = new javax.swing.JComboBox();
        jComboBox5 = new javax.swing.JComboBox();
        jComboBox6 = new javax.swing.JComboBox();
        jComboBox7 = new javax.swing.JComboBox();
        jComboBox8 = new javax.swing.JComboBox();
        jComboBox9 = new javax.swing.JComboBox();
        jPanel10 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jComboBox3 = new javax.swing.JComboBox();
        jLabel21 = new javax.swing.JLabel();
        jTextField12 = new javax.swing.JTextField();
        jButton15 = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jTextField13 = new javax.swing.JTextField();
        jTextField14 = new javax.swing.JTextField();
        jTextField15 = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jTextField16 = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jTextField17 = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jComboBox10 = new javax.swing.JComboBox();
        jPanel24 = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        jComboBox13 = new javax.swing.JComboBox();
        jButton12 = new javax.swing.JButton();
        jLabel20 = new javax.swing.JLabel();
        jDialog2 = new javax.swing.JDialog();
        jPanel9 = new javax.swing.JPanel();
        jProgressBar1 = new javax.swing.JProgressBar();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jButton13 = new javax.swing.JButton();
        jButton14 = new javax.swing.JButton();
        jDialog3 = new javax.swing.JDialog();
        jPanel12 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel13 = new javax.swing.JPanel();
        jComboBox11 = new javax.swing.JComboBox();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jComboBox12 = new javax.swing.JComboBox();
        jButton16 = new javax.swing.JButton();
        jPanel23 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        jPanel20 = new javax.swing.JPanel();
        jPanel21 = new javax.swing.JPanel();
        jPanel22 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jButton17 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jTextField8 = new javax.swing.JTextField();
        jTextField9 = new javax.swing.JTextField();
        jTextField10 = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jButton6 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();

        jButton5.setText("jButton5");

        jTextField11.setText("jTextField11");

        jButton7.setText("jButton7");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jButton11.setText("jButton11");

        jDialog1.setMinimumSize(new java.awt.Dimension(529, 704));
        jDialog1.setModal(true);
        jDialog1.setPreferredSize(new java.awt.Dimension(529, 704));

        jPanel7.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel11.setText("Compute LOD:");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel12.setText("Compute LLOQ:");

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel14.setText("Compute Linearity:");

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel15.setText("Compute Carry-0ver:");

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel16.setText("Partially Validate Specificity:");

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel17.setText("Compute ULOQ:");

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel18.setText("Peptide Result Outputs:");

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel19.setText("Log Run:");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "TRUE", "FALSE" }));

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "TRUE", "FALSE" }));

        jComboBox4.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "TRUE", "FALSE" }));

        jComboBox5.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "FALSE", "TRUE" }));

        jComboBox6.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "FALSE", "TRUE" }));

        jComboBox7.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "FALSE", "TRUE" }));

        jComboBox8.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "BOTH", "TRANSITIONS", "SUM" }));

        jComboBox9.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "TRUE" }));

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel17, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel18, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel19, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 64, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jComboBox2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jComboBox4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jComboBox5, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jComboBox9, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jComboBox8, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jComboBox7, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jComboBox6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(132, 132, 132))
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jComboBox1, jComboBox2, jComboBox4, jComboBox5, jComboBox6, jComboBox7, jComboBox8, jComboBox9});

        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(jComboBox6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(jComboBox7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(jComboBox8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(jComboBox9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jPanel10.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel13.setText("Estimate Repeatability:");

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "FALSE", "TRUE" }));

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel21.setText("Input File: ");

        jTextField12.setText("jTextField12");

        jButton15.setText("BROWSE");
        jButton15.setAlignmentY(0.0F);
        jButton15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton15ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField12, javax.swing.GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton15))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel10Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jButton15, jComboBox3});

        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton15))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel11.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel22.setText("Rpt. No Of Transitions:");

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel23.setText("Rpt. No Of Monitored Peptides:");

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel24.setText("Rpt. No Of Conc. Level:");

        jTextField13.setText("jTextField13");

        jTextField14.setText("jTextField14");

        jTextField15.setText("jTextField15");

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel25.setText("Rpt. No Of Days:");

        jTextField16.setText("jTextField16");

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel26.setText("Rpt. No Of Daily Replicates:");

        jTextField17.setText("jTextField17");

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel27.setText("Print Full view:");

        jComboBox10.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "FALSE", "TRUE" }));

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(90, 90, 90)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel22)
                    .addComponent(jLabel23)
                    .addComponent(jLabel24)
                    .addComponent(jLabel25)
                    .addComponent(jLabel26)
                    .addComponent(jLabel27))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField17)
                    .addComponent(jTextField16)
                    .addComponent(jTextField15)
                    .addComponent(jTextField14)
                    .addComponent(jTextField13)
                    .addComponent(jComboBox10, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(jComboBox10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel24.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel30.setText("Visualize Results: ");

        jComboBox13.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "TRUE", "FALSE" }));

        jButton12.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        jButton12.setText("Done");
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel24Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel30)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboBox13, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel24Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(jComboBox13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton12))
                .addGap(86, 86, 86))
        );

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(155, Short.MAX_VALUE))
        );

        jLabel20.setText("jLabel20");

        jDialog2.setMinimumSize(new java.awt.Dimension(458, 340));

        jPanel9.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel9.setMinimumSize(new java.awt.Dimension(438, 320));

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jButton13.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        jButton13.setText("Cancel");
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });

        jButton14.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        jButton14.setText("Close");
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton14ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
                    .addComponent(jProgressBar1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton14)))
                .addContainerGap())
        );

        jPanel9Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jButton13, jButton14});

        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton13)
                    .addComponent(jButton14))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel9Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jButton13, jButton14});

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(57, Short.MAX_VALUE))
        );

        jDialog3.setMinimumSize(new java.awt.Dimension(750, 600));

        jPanel12.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel12.addContainerListener(new java.awt.event.ContainerAdapter() {
            public void componentRemoved(java.awt.event.ContainerEvent evt) {
                jPanel12ComponentRemoved(evt);
            }
        });

        jPanel13.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jComboBox11.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel28.setText("Peptide-transition:");

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel29.setText("Experiment: ");

        jComboBox12.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Response", "Repeatability" }));

        jButton16.setText("View");

        jPanel23.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 296, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel28)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBox11, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel29)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox12, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton16, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28)
                    .addComponent(jLabel29)
                    .addComponent(jComboBox12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton16))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Peptides", jPanel13);

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 661, Short.MAX_VALUE)
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 359, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Response.LOD", jPanel15);

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 661, Short.MAX_VALUE)
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 359, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Response.LLOQ.CV", jPanel16);

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 661, Short.MAX_VALUE)
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 359, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Response.ULOQ.CV", jPanel17);

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 661, Short.MAX_VALUE)
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 359, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Response.PVSpec.MaxDev", jPanel18);

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 661, Short.MAX_VALUE)
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 359, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Repeat.Lo.CV", jPanel19);

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 661, Short.MAX_VALUE)
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 359, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Repeat.Me.CV", jPanel20);

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 661, Short.MAX_VALUE)
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 359, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Repeat.Hi.CV", jPanel21);

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 661, Short.MAX_VALUE)
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 359, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Repeat.PVSpec.MaxDev", jPanel22);

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 403, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel14.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jButton17.setText("Close");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton17, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton17)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jDialog3Layout = new javax.swing.GroupLayout(jDialog3.getContentPane());
        jDialog3.getContentPane().setLayout(jDialog3Layout);
        jDialog3Layout.setHorizontalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel14, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jDialog3Layout.setVerticalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel1.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        jLabel1.setText("Preprocessed File:");

        jLabel2.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        jLabel2.setText("Metadata File:");

        jLabel3.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        jLabel3.setText("Dilution File:");

        jLabel4.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        jLabel4.setText("Output(s) Directory:");

        jTextField1.setText("jTextField1");

        jTextField2.setText("jTextField2");

        jTextField3.setText("jTextField3");

        jTextField4.setText("jTextField4");

        jButton1.setText("Browse");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Browse");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Browse");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Browse");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField1)
                            .addComponent(jTextField4)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField2))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2)
                    .addComponent(jButton1)
                    .addComponent(jButton3)
                    .addComponent(jButton4))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton4))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel5.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        jLabel5.setText("Peptides Monitored:");

        jLabel6.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        jLabel6.setText("Transitions:");

        jLabel7.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        jLabel7.setText("Precurve Blanks:");

        jLabel8.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        jLabel8.setText("Total Blanks:");

        jLabel9.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        jLabel9.setText("Replicates:");

        jLabel10.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        jLabel10.setText("Serial Dilutions:");

        jTextField5.setText("jTextField5");

        jTextField6.setText("jTextField6");

        jTextField7.setText("jTextField7");
        jTextField7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField7ActionPerformed(evt);
            }
        });

        jTextField8.setText("jTextField8");

        jTextField9.setText("jTextField9");

        jTextField10.setText("jTextField10");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel5)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(66, 66, 66))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jTextField10, jTextField5, jTextField6, jTextField7, jTextField8, jTextField9});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jButton6.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        jButton6.setText("More Options");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton6)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton6)
                .addContainerGap())
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jButton8.setText("Reset");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton9.setText("Cancel");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jButton10.setText("Execute");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton10)
                .addContainerGap())
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jButton10, jButton8, jButton9});

        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton10)
                    .addComponent(jButton9)
                    .addComponent(jButton8))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jButton10, jButton8, jButton9});

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField7ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        // "More Options" action button....
        jDialog1.setVisible(true);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        // "Done" button of jDialog1 (More options dialog)
        jDialog1.setVisible(false);
        
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        try {
            // TODO add your handling code here:
            // "Reset" Button
            // reset choosen values to default settings
            setConfigurations();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();;
            Logger.getLogger(MRMPlusGUI.class.getName()).log(Level.SEVERE, null, ex);
            
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(MRMPlusGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here:
        // "Cancel" button...
        System.out.println("Cancelling...");
        System.exit(1);
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // Select preprocessed file...
        OneFileChooser chooser = new OneFileChooser("Select preprocessed file...", currentDir);
        if(chooser.getInputFile() != null){
            String input = chooser.getInputFile();
            jTextField1.setText(input);
            currentDir = new File(input).getParent(); // keep track of last directory...           
        }        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // Select metadata file...
        OneFileChooser chooser = new OneFileChooser("Select metadata file...", currentDir);
        if(chooser.getInputFile() != null){
            String input = chooser.getInputFile();
            jTextField2.setText(input);
            currentDir = new File(input).getParent(); // keep track of last directory...           
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // Select dilution file...
        OneFileChooser chooser = new OneFileChooser("Select dilution file...", currentDir);
        if(chooser.getInputFile() != null){
            String input = chooser.getInputFile();
            jTextField3.setText(input);
            currentDir = new File(input).getParent(); // keep track of last directory...           
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // Select output(s) location...
        OutputDirChooser chooser = new OutputDirChooser("Select output(s) location...", currentDir);
        if(chooser.getOutputDir() != null){
            String output = chooser.getOutputDir();
            jTextField4.setText(output);
            currentDir = new File(output).getParent(); // keep track of last directory...           
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        
        try{
            // "Execute" button
            // set configuration(s) [from user specified values]
            setUserConfigurations();
            // Initiate SwingWorker 
            // run program...
            // set JProgressBar bound values....
            jProgressBar1.setMinimum(0);
            jProgressBar1.setMaximum(100);
            jButton14.setEnabled(false);
            // instantiate xGlycScan object
            MRMPlus mrmplus = null;
            try {
                mrmplus = new MRMPlus(jTextArea1, config);
            } catch (Exception ex) {
                Logger.getLogger(MRMPlus.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            } 
            mrmplus.addPropertyChangeListener(new PropertyChangeListener(){
                @Override
                public  void propertyChange(PropertyChangeEvent evt) {               
                    if ("progress".equals(evt.getPropertyName())) {
                        if((Integer)evt.getNewValue() == 0){
                        jProgressBar1.setIndeterminate(true);
                        }else{
                            jProgressBar1.setIndeterminate(false);
                            jProgressBar1.setValue((Integer)evt.getNewValue());
                        }
                        if((Integer)evt.getNewValue() == 100){
                            //System.out.println("\n...Done Executing!!!");
                            //jTextArea1.append("\n...Done!!!");
                            JOptionPane.showMessageDialog(jDialog2, "...Complete!", "Complete!", JOptionPane.OK_OPTION);            
                            jButton14.setEnabled(true);

                        }

                    }            
                }
            });
            mrmplus.execute();
            jDialog2.setVisible(true);            
        } catch(Exception ex){
            ex.printStackTrace();
        }   
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton14ActionPerformed
        // TODO add your handling code here:
        // "Close" button for monitor Dialog
        jDialog2.setVisible(false);
        
    }//GEN-LAST:event_jButton14ActionPerformed

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
        // TODO add your handling code here:
        // "Cancel" button for monitor Dialog
        System.exit(1);
    }//GEN-LAST:event_jButton13ActionPerformed

    private void jButton15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton15ActionPerformed
        // TODO add your handling code here:
        // Experiment II input file chooser 
        // Select preprocessed file...
        OneFileChooser chooser = new OneFileChooser("Select Repeatability Experiment preprocessed file...", currentDir);
        if(chooser.getInputFile() != null){
            String input = chooser.getInputFile();
            jTextField12.setText(input);
            currentDir = new File(input).getParent(); // keep track of last directory...           
        }    
    }//GEN-LAST:event_jButton15ActionPerformed

    private void jPanel12ComponentRemoved(java.awt.event.ContainerEvent evt) {//GEN-FIRST:event_jPanel12ComponentRemoved
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanel12ComponentRemoved

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MRMPlusGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MRMPlusGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MRMPlusGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MRMPlusGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                try {
                    new MRMPlusGUI().setVisible(true);
                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                    Logger.getLogger(MRMPlusGUI.class.getName()).log(Level.SEVERE, null, ex);
                    
                } catch (IOException ex) {
                    ex.printStackTrace();
                    Logger.getLogger(MRMPlusGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton16;
    private javax.swing.JButton jButton17;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox10;
    private javax.swing.JComboBox jComboBox11;
    private javax.swing.JComboBox jComboBox12;
    private javax.swing.JComboBox jComboBox13;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JComboBox jComboBox3;
    private javax.swing.JComboBox jComboBox4;
    private javax.swing.JComboBox jComboBox5;
    private javax.swing.JComboBox jComboBox6;
    private javax.swing.JComboBox jComboBox7;
    private javax.swing.JComboBox jComboBox8;
    private javax.swing.JComboBox jComboBox9;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JDialog jDialog3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField12;
    private javax.swing.JTextField jTextField13;
    private javax.swing.JTextField jTextField14;
    private javax.swing.JTextField jTextField15;
    private javax.swing.JTextField jTextField16;
    private javax.swing.JTextField jTextField17;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    // End of variables declaration//GEN-END:variables

    // Specified variables
    private HashMap<String, String> config; //configuration file...
    private String currentDir = System.getProperty("user.dir"); //current directory tracker... 

    

    //public class MRMPlus extends SwingWorker<Void,String>{ //JFreeChart
    public class MRMPlus extends SwingWorker<MRMPlusResultCharts,String>{    
        private HashMap<String, String> configFile = null;
        private JTextArea textArea = null;
        
        protected MRMPlus(JTextArea textArea, HashMap<String, String> config){
            // this only initializes the SwingWorker class MRMPlus, the bulk of the work is 
            // done in the doInBackground() method
            this.configFile = config;
            this.textArea = textArea;
        }
        
        // dummy method...
        protected JTextArea getJTextArea(){
            return textArea;
        }
        // dummy method...
        protected HashMap<String, String> getConfig(){
            return configFile;
        }
        
        @Override
        protected void process(List<String> messages){
            for(String message : messages){
                textArea.append(message + "\n");
            }           
        } 
        
        @Override
        protected void done(){
            System.out.println("\nUpdating GUI components...");
            //try {
                /*
                ChartPanel rLODChartPanel = new ChartPanel(get());
                //rLODChartPanel.setSize(jPanel15.getSize());
                //rLODChartPanel.setBorder(BorderFactory.createTitledBorder("Test"));
                jPanel15.add(rLODChartPanel, BorderLayout.CENTER);
                jPanel15.validate();
                jPanel15.setVisible(true);
                jDialog3.setVisible(true);
                */
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            
                            new MRMPlusResultChartsDisplay(get()).display();
                        
                        
                        } catch (InterruptedException ex) {
                            Logger.getLogger(MRMPlusGUI.class.getName()).log(Level.SEVERE, null, ex);
                            ex.printStackTrace();
                        } catch (ExecutionException ex) {
                            Logger.getLogger(MRMPlusGUI.class.getName()).log(Level.SEVERE, null, ex);
                            ex.printStackTrace();
                        }
                    }
                });
                
            //} catch (InterruptedException ex) {
            //    Logger.getLogger(MRMPlusGUI.class.getName()).log(Level.SEVERE, null, ex);
            //    ex.printStackTrace();
            //} catch (ExecutionException ex) {
            //    Logger.getLogger(MRMPlusGUI.class.getName()).log(Level.SEVERE, null, ex);
            //    ex.printStackTrace();
            //}
        }
        
        @Override
        //protected Void doInBackground() throws Exception {
        protected MRMPlusResultCharts doInBackground() throws Exception {    
            
            HashMap<String, JFreeChart> generalResultCharts = null;
            HashMap<String, JFreeChart> peptideResponseResultCharts = null;
            HashMap<String, JFreeChart> peptideRepeatResultCharts = null;
            
            MRMPlusResultCharts resultCharts = null;
            
            MRMPlusPlotDataCollection plotDataCollection = new MRMPlusPlotDataCollection();
            
            try{
                setProgress(5);
                LinkedList<PeptideRecord> peptideRecords;
                HashMap<String, LinkedList<PeptideRecord>> pepToRecordsMap;

                long start_time = new Date().getTime();
                //instantiate a loggerFile
                String outputId = String.valueOf(start_time);
                String logFile = "." + File.separator + "logs" + File.separator + outputId + ".response.log"; 
                //File loggerFile = new File(logFile);
                PrintWriter logWriter = new PrintWriter(logFile);

                //read inputFile...
                System.out.println("Reading 'skyline' preprocessed data input file...");
                logWriter.println("Reading 'skyline' preprocessed data input file...");
                publish("Reading 'skyline' preprocessed data input file...");
                InputFileReader inFileReader = new InputFileReader();
                String inputFile = configFile.get("preprocessedFile"); //can be alternative derived 
                peptideRecords = inFileReader.readInputFile(inputFile, configFile, logWriter);
                //setProgress(2);

                //associate metadata-info with peptide records
                // Get associated information - metadata...
                System.out.println("Reading metadata file...");
                logWriter.println("Reading metadata file...");
                publish("Reading metadata file...");
                String metadataFile = configFile.get("metadataFile");
                MetadataFileReader metadataFileReader = new MetadataFileReader();
                LinkedList<MRMRunMeta> metadata = metadataFileReader.readFile(metadataFile, logWriter);


                // map replicate name to meta-info; the replicate name is used in this case because it is unique and could
                // be used to associate metadata to peptide record as we'll use subsequently...
                System.out.println("Mapping 'replicateName'attribute to 'MRMRunMeta' object...");
                logWriter.println("Mapping 'replicateName'attribute to 'MRMRunMeta' object...");
                publish("Mapping 'replicateName'attribute to 'MRMRunMeta' object...");
                ExpIMetadataMapper metadatamapper = new ExpIMetadataMapper();
                HashMap<String, MRMRunMeta> replicateNameToMetadataMap = 
                        metadatamapper.mapReplicateNameToMetadata(metadata);
                logWriter.println("  " + replicateNameToMetadataMap.keySet().size() + " MRMRunMeta objects found in metadata file...");
                setProgress(10);
                //publish();

                // update peptideRecords with metadata info...
                System.out.println("Updating peptide records...");
                logWriter.println("Updating peptide records...");
                publish("Updating peptide records...");
                PeptideRecordsUpdater updater = new PeptideRecordsUpdater();
                updater.updatePeptideRecords(peptideRecords, replicateNameToMetadataMap, logWriter);
                setProgress(15);

                // get associated [spikedIn] serial dilution concentration...
                System.out.println("Reading dilution file...");
                logWriter.println("Reading dilution file...");
                publish("Reading dilution file...");
                String dilutionFile = configFile.get("dilutionFile");
                DilutionFileReader dilFileReader = new DilutionFileReader();
                HashMap<String, Double> pointToDilutionMap = dilFileReader.readFile(dilutionFile, logWriter);
                setProgress(20);

                // update respective peptideRecord with associated point dilution
                System.out.println("Updating peptide records...");
                logWriter.println("Updating peptide records...");
                publish("Updating peptide records...");
                updater.updatePeptideRecordsDilutions(peptideRecords, pointToDilutionMap, logWriter);
                setProgress(30);


                //map peptides to records...
                System.out.println("Mapping peptide sequence to peptide record...");
                logWriter.println("Mapping peptide sequence to peptide record...");
                publish("Mapping peptide sequence to peptide record...");
                PeptideToRecordsMapper mapper = new PeptideToRecordsMapper();
                pepToRecordsMap = mapper.mapPeptideToRecord(peptideRecords);
                setProgress(40);

                // ------ Compute statistics/results ------ // 
                
                // **************************************** //
                //              Experiment I....
                // **************************************** //
                System.out.println("Estimating MRMPlus QCs...");
                logWriter.println("Estimating MRMPlus QCs...");
                publish("Estimating MRMPlus QCs...");
                PeptideQCEstimator qcEstimator = new PeptideQCEstimator();
                setProgress(0);
                HashMap<String, LinkedList<PeptideResult>> peptideQCEstimates = 
                    qcEstimator.estimatePeptidesQCs(pepToRecordsMap, 
                                                pointToDilutionMap, 
                                                    configFile, 
                                                        logWriter, plotDataCollection);
                setProgress(75);

                //Print results
                System.out.println("Printing MRMPlus QC estimates...");
                logWriter.println("Printing MRMPlus QC estimates...");
                publish("Printing MRMPlus QC estimates...");
                QCEstimatesPrinter printer = new QCEstimatesPrinter();
                printer.printMRMPlusEstimates(peptideQCEstimates, configFile, outputId);
                setProgress(90);
                
                // **************************************** //
                //              Experiment II
                // **************************************** //
                PeptideRepeatabilityMiniValidator prmValidator = null;
                HashMap<String, ExpIIMiniValidationOfRepeatabilityResult> peptideRepeatabilityValidationResultMap = null;
                // computing values for experiment 2 (mini-validation of repeatability....
                if(configFile.get("computeMiniValidationOfRepeatability").equalsIgnoreCase("TRUE")){
                    publish("Validating repeatability...");
                    prmValidator = new PeptideRepeatabilityMiniValidator();
                    peptideRepeatabilityValidationResultMap = 
                            prmValidator.validateRepeatability(configFile, outputId, plotDataCollection);
                    
                    //print output results...
                    setProgress(95);
                    publish("Printing repeatability...");
                    RepeatabilityEstimatesPrinter rprinter = new RepeatabilityEstimatesPrinter();
                    rprinter.printMRMPlusRepeatabilityEstimates(peptideRepeatabilityValidationResultMap, configFile, outputId);    
                }

                System.out.println("...Done!!!");
                logWriter.println("...Done!!!");
                publish("...Done!!!");


                Date end_time = new Date();
                long end = end_time.getTime();
                // for logger...
                logWriter.println("End: " + end + ": " + end_time.toString());
                logWriter.println("Total time: " + (end - start_time) + " milliseconds; " + 
                                TimeUnit.MILLISECONDS.toMinutes(end - start_time) + " min(s), "
                                + (TimeUnit.MILLISECONDS.toSeconds(end - start_time) - 
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(end - start_time))) + " seconds.");
                // for GUI...
                publish("End: " + end + ": " + end_time.toString());
                publish("Total time: " + (end - start_time) + " milliseconds; " + 
                                TimeUnit.MILLISECONDS.toMinutes(end - start_time) + " min(s), "
                                + (TimeUnit.MILLISECONDS.toSeconds(end - start_time) - 
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(end - start_time))) + " seconds.");



                
                setProgress(100);
                
                
                if(configFile.get("runVisualization").equalsIgnoreCase("TRUE")){
                    
                    //MRMPlusResultCharts charts = new MRMPlusResultCharts();
                    generalResultCharts = new HashMap<String, JFreeChart>();
                    //peptideResponseResultCharts = new HashMap<String, JFreeChart>();
                    //peptideRepeatResultCharts = new HashMap<String, JFreeChart>();
                    
                    System.out.println("\n");
                    logWriter.println("\n");
                    publish("\n");
                    
                    System.out.println("============================");
                    logWriter.println("============================");
                    publish("============================");
                    
                    
                    System.out.println("Running visualization modules");
                    logWriter.println("Running visualization modules");
                    publish("Running visualization modules");
                    // **************************************** //
                    //      Plot Visualizations...
                    // **************************************** //

                    // plot...
                    // (A) Individual:
                    //      (1) Peptide-transition performance (interactive) visulization...
                                // on jPanel13
                                // sort peptides-transitions
                    // (B) All together:
                    //      (2) Response.LOD - jPanel15
                                // get and create the Dataset (CategoryDataset) object...
                                // create Chart
                                // append ChartPanel
                    
                    //System.out.println("Creating JFreeChart Response LOD Dataset");
                    //logWriter.println("Creating JFreeChart Response LOD Dataset");
                    //publish("Creating JFreeChart Response LOD Dataset");
                    
                    JFreeChart reponseLODLineChart = 
                            ChartFactory.createLineChart(
                                 "","Peptide-transition", "Limit Of Detection (LOD)",
                                 createResponseChartDataset(peptideQCEstimates,
                                                PeptideResultsPlotType.RESPONSE_LOD),
                                 PlotOrientation.VERTICAL,
                                 true,true,false);
                    
                    generalResultCharts.put("reponse.LOD", reponseLODLineChart);
                    
                    /*
                    System.out.println("Creating JFreeChart Response LOD Panel");
                    logWriter.println("RCreating JFreeChart Response LOD Panel");
                    publish("Creating JFreeChart Response LOD Panel");
                    
                    ChartPanel rLODChartPanel = new ChartPanel(lineChartReponseLOD);
                    //rLODChartPanel.setSize(jPanel15.getSize());
                    //rLODChartPanel.setBorder(BorderFactory.createTitledBorder("Test"));
                    jPanel15.add(rLODChartPanel, BorderLayout.CENTER);
                    jPanel15.validate();
                    jPanel15.setVisible(true);
                    */
                                
                    //      (3) Response.LLOQ.CV - jPanel16
                    JFreeChart reponseLLOQCVLineChart = 
                            ChartFactory.createLineChart(
                                 "","Peptide-transition", "LLOQ CV",
                                 createResponseChartDataset(peptideQCEstimates,
                                                PeptideResultsPlotType.RESPONSE_LLOQ_CV),
                                 PlotOrientation.VERTICAL,
                                 true,true,false);
                    generalResultCharts.put("response.LLOQ.CV",reponseLLOQCVLineChart );
                    
                    //      (4) Response.ULOQ.CV - jPanel17 
                    JFreeChart reponseULOQCVLineChart = 
                            ChartFactory.createLineChart(
                                 "","Peptide-transition", "ULOQ CV",
                                 createResponseChartDataset(peptideQCEstimates,
                                                PeptideResultsPlotType.RESPONSE_ULOQ_CV),
                                 PlotOrientation.VERTICAL,
                                 true,true,false);
                    generalResultCharts.put("response.ULOQ.CV", reponseULOQCVLineChart);
                    
                    //      (5) Response.PVSpec.MaxDev - jPanel18
                    JFreeChart reponsePVSpecMaxDevLineChart = 
                            ChartFactory.createLineChart(
                                 "","Peptide-transition", "PVSpec Max. Dev.",
                                 createResponseChartDataset(peptideQCEstimates,
                                                PeptideResultsPlotType.RESPONSE_PVS_MaxDev),
                                 PlotOrientation.VERTICAL,
                                 true,true,false);
                    generalResultCharts.put("response.PVSpec.MaxDev", reponsePVSpecMaxDevLineChart);
                    
                    
                    // ******************************************************* //
                    
                    //      (6) Repeat.Lo.CV - jPanel19
                    JFreeChart repeatLoCVLineChart = 
                            ChartFactory.createLineChart(
                                 "","Peptide-transition", "Lower Conc CVs",
                                 createRepeatChartDataset(peptideRepeatabilityValidationResultMap,
                                                PeptideResultsPlotType.REPEAT_Lo_CV),
                                 PlotOrientation.VERTICAL,
                                 true,true,false);
                    generalResultCharts.put("repeat.Lo.CV", repeatLoCVLineChart);
                    
                    //      (7) Repeat.Me.CV - jPanel20
                    JFreeChart repeatMeCVLineChart = 
                            ChartFactory.createLineChart(
                                 "","Peptide-transition", "Medium Conc CVs",
                                 createRepeatChartDataset(peptideRepeatabilityValidationResultMap,
                                                PeptideResultsPlotType.REPEAT_Me_CV),
                                 PlotOrientation.VERTICAL,
                                 true,true,false);
                    generalResultCharts.put("repeat.Me.CV", repeatMeCVLineChart);
                    
                    //      (8) Repeat.Hi.CV - jPanel21
                    JFreeChart repeatHiCVLineChart = 
                            ChartFactory.createLineChart(
                                 "","Peptide-transition", "High Conc CVs",
                                 createRepeatChartDataset(peptideRepeatabilityValidationResultMap,
                                                PeptideResultsPlotType.REPEAT_Hi_CV),
                                 PlotOrientation.VERTICAL,
                                 true,true,false);
                    generalResultCharts.put("repeat.Hi.CV", repeatHiCVLineChart );
                    
                    //      (9) Repeat.PVSpec.MaxDev - jPanel22
                    JFreeChart repeatMaxDevLineChart = 
                            ChartFactory.createLineChart(
                                 "","Peptide-transition", "PVSpec Max. Dev.",
                                 createRepeatChartDataset(peptideRepeatabilityValidationResultMap,
                                                PeptideResultsPlotType.REPEAT_PVS_MaxDev),
                                 PlotOrientation.VERTICAL,
                                 true,true,false);
                    generalResultCharts.put("repeat.PVSpec.MaxDev", repeatMaxDevLineChart );
                    
                    
                    // generate response peptideResultCharts
                    peptideResponseResultCharts = generateResponsePeptideResultCharts(plotDataCollection);
                    // generate repeatability peptideResultCharts
                    peptideRepeatResultCharts = generateReapetabilityPeptideResultCharts(plotDataCollection);
                    
                    // instantiate 'MRMPlusResultCharts' 
                    resultCharts = new MRMPlusResultCharts(generalResultCharts,
                                    peptideResponseResultCharts, peptideRepeatResultCharts);
                    
                }    
                logWriter.close();               
                
            }catch(Exception ex){
                ex.printStackTrace();
            }
            return resultCharts;
        }
     
        private CategoryDataset createResponseChartDataset(HashMap<String, 
                                                    LinkedList<PeptideResult>> peptideQCEstimates, 
                                              PeptideResultsPlotType peptideResultsPlotType) {
            // throw new UnsupportedOperationException("Not supported yet."); 
            //To change body of generated methods, choose Tools | Templates.
            
            DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
            
            
            Set<String> peptideSequences = peptideQCEstimates.keySet();
            for(String peptideSequence : peptideSequences){
                LinkedList<PeptideResult> peptideResults = peptideQCEstimates.get(peptideSequence);
                for(PeptideResult peptideResult : peptideResults){
                    //printPeptideRowResult(peptideResult, printer, config);
                    String transitionId = peptideResult.getTransitionID();
                    String id = peptideSequence + "_" + transitionId;
                    
                    switch(peptideResultsPlotType){
                        //case RESPONSE_LOD:
                        case RESPONSE_LLOQ_CV:
                            double lloqCV;
                            LowerLimitOfQuantification lloq = null;
                            CoefficientOfVariation cv = null;
                            
                            lloqCV = 100;
                            lloq = peptideResult.getLowerLimitOfQuantification();
                            if(lloq!=null)
                               cv = lloq.getCoefficientOfVariation();
                            if(cv!=null)
                               lloqCV = cv.getCoef();
                               //peptideResult.getLowerLimitOfQuantification().getCoefficientOfVariation().getCoef();
                            dataset.addValue(lloqCV, "Response LLOQ CV", id);
                            break;
                            
                        case RESPONSE_ULOQ_CV:
                            double uloqCV;
                            UpperLimitOfQuantification uloq = null;
                            CoefficientOfVariation uloqcv = null;
                            
                            uloqCV = 100;
                            uloq = peptideResult.getUpperLimitOfQuantification();
                            if(uloq != null)
                               uloqcv = uloq.getCoefficientOfVariation();
                            if(uloqcv != null)
                               uloqCV = uloqcv.getCoef();
                               //peptideResult.getUpperLimitOfQuantification().getCoefficientOfVariation().getCoef();
                            dataset.addValue(uloqCV, "Response ULOQ CV", id);
                            break;
                            
                        case RESPONSE_PVS_MaxDev:
                            double maxDev = 100;
                            PartialValidationOfSpecificity pvs;
                            
                            pvs = peptideResult.getPartialValidationOfSpecificity();
                            if(pvs != null)
                                maxDev = pvs.getMaxPeakRatioDevFromMean();
                                    //peptideResult.getPartialValidationOfSpecificity().getMaxPeakRatioDevFromMean();
                            dataset.addValue(maxDev, "Response PVSpec Max. Dev.", id);
                            break;
                                                                   
                        default: //case RESPONSE_LOD:
                            double lod = peptideResult.getLimitOfDetection().getLimitOfDetection();
                            dataset.addValue(lod, "Response LOD", id);
                            //System.out.println(lod + "\t" + "Response LOD" + "\t" + id);
                            break;
                    }
                }
            }
            
            return dataset;
        }

        private CategoryDataset createRepeatChartDataset(HashMap<String, 
                ExpIIMiniValidationOfRepeatabilityResult> peptideRepeatabilityValidationResultMap, 
                      PeptideResultsPlotType peptideResultsPlotType) {
            //throw new UnsupportedOperationException("Not supported yet."); 
            //To change body of generated methods, choose Tools | Templates.
            DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
            
            //Set<String> concLevels = getConcentrationLevels(peptideRepeatabilityValidationResultMap);
            
            Set<String> peptideSequences = peptideRepeatabilityValidationResultMap.keySet();
            for(String peptideSequence : peptideSequences){
                //for each peptide Sequence
                ExpIIMiniValidationOfRepeatabilityResult peptideAssResult = 
                        peptideRepeatabilityValidationResultMap.get(peptideSequence);
                HashMap<String, ExpIIPeptideValidatedLLOQ> subIdsToValidatedLLOQMap = 
                        peptideAssResult.getSubIdsToValidatedLLOQMap();
                HashMap<String, ExpIIPeptidePartialValidationOfSpecificity>  subIdsToPartialValidationOfSpecMap = 
                        peptideAssResult.getSubIdsToPartialValidationOfSpecMap();
                
                //print summed Associated values..
                //print peptideSequence
                //printer.print(peptideSequence + "\t");
                // print summed-transition associated variables
                //printer.print("SUMMED" + "\t");
                
                // ******* CV Values ******* //
                HashMap<String, ExpIIValidationIdAndConcLevelsCoefs> subIdsToConcLevelsCoefsMap; 
                ExpIIValidationIdAndConcLevelsCoefs subIdAndConcLevelsCoefs;        
                HashMap<String, ExpIIConcLevelCoefficientOfVariation> concLevelToCoeffficientsMap;
                
                ExpIIConcLevelCoefficientOfVariation concLevelCoefs;
                String id;
                Set<String> subIds;
                
                double intraAssayCV = 100;
                double interAssayCV = 100;
                double totalAssayCV = 100;
                
                switch(peptideResultsPlotType){
                    
                    case REPEAT_PVS_MaxDev:
                        // get values the SUMMED transitions of this peptide.
                        // since PVSpec.Max.Dev are transition attributes, it is not applicable 
                        //    to compute such on the SUMMED values therefore
                        id = peptideSequence + "_" + "SUMMED";
                        dataset.addValue(0.0000, "PVSpec Max Dev", id);
                        
                        //get values for other assayed transitions of this peptide.
                        subIds = subIdsToPartialValidationOfSpecMap.keySet();
                        for(String subId : subIds){
                            id = peptideSequence + "_" + subId;
                            if(subId.equalsIgnoreCase("SUMMED")==false){
                                ExpIIPeptidePartialValidationOfSpecificity pVSpec = 
                                    subIdsToPartialValidationOfSpecMap.get(subId);
                                dataset.addValue(pVSpec.getMaxDeviation(),"PVSpec Max Dev", id);
                            }
                        }
                                                
                        break;
                              
                    case REPEAT_Lo_CV:
                        
                        // get values (intraAssay, interAssayCV, and totalCV) for the SUMMED 
                        //  transitions of this peptide.
                        subIdsToConcLevelsCoefsMap = peptideAssResult.getSubIdsToConcLevelsCoefsMap(); 
                        subIdAndConcLevelsCoefs = subIdsToConcLevelsCoefsMap.get("SUMMED");
                        concLevelToCoeffficientsMap = subIdAndConcLevelsCoefs.getConcLevelToCoeffficientsMap();
                        
                        concLevelCoefs = concLevelToCoeffficientsMap.get("Lo");                       
                        id = peptideSequence + "_" + "SUMMED";
                        
                        if(concLevelCoefs != null){
                            intraAssayCV = concLevelCoefs.getIntraAssayCV();
                            interAssayCV = concLevelCoefs.getInterAssayCV();
                            totalAssayCV = concLevelCoefs.getTotalCV();
                        }
                        dataset.addValue(intraAssayCV,"intraAssayCV", id);
                        dataset.addValue(interAssayCV,"interAssayCV", id);
                        dataset.addValue(totalAssayCV,"totalCV", id);
                        
                        //get values (intraAssay, interAssayCV, and totalCV) for other assayed 
                        //  transitions of this peptide.
                        subIds = subIdsToPartialValidationOfSpecMap.keySet();
                        for(String subId : subIds){
                            if(subId.equalsIgnoreCase("SUMMED")==false){
                                id = peptideSequence + "_" + subId;
                                
                                // ***** CV Values ***** //
                                ExpIIValidationIdAndConcLevelsCoefs transitonSubIdAndConcLevelsCoefs = 
                                    subIdsToConcLevelsCoefsMap.get(subId);        
                                HashMap<String, ExpIIConcLevelCoefficientOfVariation> transitonSubIdConcLevelToCoeffficientsMap =
                                        transitonSubIdAndConcLevelsCoefs.getConcLevelToCoeffficientsMap();
                                
                                concLevelCoefs = transitonSubIdConcLevelToCoeffficientsMap.get("Lo");
                                if(concLevelCoefs != null){
                                    intraAssayCV = concLevelCoefs.getIntraAssayCV();
                                    interAssayCV = concLevelCoefs.getInterAssayCV();
                                    totalAssayCV = concLevelCoefs.getTotalCV();
                                }
                                dataset.addValue(intraAssayCV,"intraAssayCV", id);
                                dataset.addValue(interAssayCV,"interAssayCV", id);
                                dataset.addValue(totalAssayCV,"totalCV", id);
                                                                         
                            }
                        }
                        break;

                    case REPEAT_Me_CV:
                        
                        //get values (intraAssay, interAssayCV, and totalCV) for the SUMMED 
                        //  transitions of this peptide.
                        subIdsToConcLevelsCoefsMap = peptideAssResult.getSubIdsToConcLevelsCoefsMap(); 
                        subIdAndConcLevelsCoefs = subIdsToConcLevelsCoefsMap.get("SUMMED");
                        concLevelToCoeffficientsMap = subIdAndConcLevelsCoefs.getConcLevelToCoeffficientsMap();
                        
                        concLevelCoefs = concLevelToCoeffficientsMap.get("Me");                       
                        id = peptideSequence + "_" + "SUMMED";
                        if(concLevelCoefs != null){
                            intraAssayCV = concLevelCoefs.getIntraAssayCV();
                            interAssayCV = concLevelCoefs.getInterAssayCV();
                            totalAssayCV = concLevelCoefs.getTotalCV();
                        }
                        dataset.addValue(intraAssayCV,"intraAssayCV", id);
                        dataset.addValue(interAssayCV,"interAssayCV", id);
                        dataset.addValue(totalAssayCV,"totalCV", id);
                        
                        //get values (intraAssay, interAssayCV, and totalCV) for other assayed 
                        //  transitions of this peptide.
                        subIds = subIdsToPartialValidationOfSpecMap.keySet();
                        for(String subId : subIds){
                            if(subId.equalsIgnoreCase("SUMMED")==false){
                                id = peptideSequence + "_" + subId;
                                
                                // ***** CV Values ***** //
                                ExpIIValidationIdAndConcLevelsCoefs transitonSubIdAndConcLevelsCoefs = 
                                    subIdsToConcLevelsCoefsMap.get(subId);        
                                HashMap<String, ExpIIConcLevelCoefficientOfVariation> transitonSubIdConcLevelToCoeffficientsMap =
                                        transitonSubIdAndConcLevelsCoefs.getConcLevelToCoeffficientsMap();
                                
                                concLevelCoefs = transitonSubIdConcLevelToCoeffficientsMap.get("Me");
                                if(concLevelCoefs != null){
                                    intraAssayCV = concLevelCoefs.getIntraAssayCV();
                                    interAssayCV = concLevelCoefs.getInterAssayCV();
                                    totalAssayCV = concLevelCoefs.getTotalCV();
                                }
                                dataset.addValue(intraAssayCV,"intraAssayCV", id);
                                dataset.addValue(interAssayCV,"interAssayCV", id);
                                dataset.addValue(totalAssayCV,"totalCV", id);
                                                                        
                            }
                        }
                        break;

                    default: //case REPEAT_Hi_CV:
                        
                        //get values (intraAssay, interAssayCV, and totalCV) for the SUMMED 
                        //  transitions of this peptide.
                        subIdsToConcLevelsCoefsMap = peptideAssResult.getSubIdsToConcLevelsCoefsMap(); 
                        subIdAndConcLevelsCoefs = subIdsToConcLevelsCoefsMap.get("SUMMED");
                        concLevelToCoeffficientsMap = subIdAndConcLevelsCoefs.getConcLevelToCoeffficientsMap();
                        
                        concLevelCoefs = concLevelToCoeffficientsMap.get("Hi");                       
                        id = peptideSequence + "_" + "SUMMED";
                        if(concLevelCoefs != null){
                            intraAssayCV = concLevelCoefs.getIntraAssayCV();
                            interAssayCV = concLevelCoefs.getInterAssayCV();
                            totalAssayCV = concLevelCoefs.getTotalCV();
                        }
                        dataset.addValue(intraAssayCV,"intraAssayCV", id);
                        dataset.addValue(interAssayCV,"interAssayCV", id);
                        dataset.addValue(totalAssayCV,"totalCV", id);
                        
                        //get values (intraAssay, interAssayCV, and totalCV) for other assayed 
                        //  transitions of this peptide.
                        subIds = subIdsToPartialValidationOfSpecMap.keySet();
                        for(String subId : subIds){
                            if(subId.equalsIgnoreCase("SUMMED")==false){
                                id = peptideSequence + "_" + subId;
                                
                                // ***** CV Values ***** //
                                ExpIIValidationIdAndConcLevelsCoefs transitonSubIdAndConcLevelsCoefs = 
                                    subIdsToConcLevelsCoefsMap.get(subId);        
                                HashMap<String, ExpIIConcLevelCoefficientOfVariation> transitonSubIdConcLevelToCoeffficientsMap =
                                        transitonSubIdAndConcLevelsCoefs.getConcLevelToCoeffficientsMap();
                                
                                concLevelCoefs = transitonSubIdConcLevelToCoeffficientsMap.get("Hi");
                                if(concLevelCoefs != null){
                                    intraAssayCV = concLevelCoefs.getIntraAssayCV();
                                    interAssayCV = concLevelCoefs.getInterAssayCV();
                                    totalAssayCV = concLevelCoefs.getTotalCV();
                                }
                                dataset.addValue(intraAssayCV,"intraAssayCV", id);
                                dataset.addValue(interAssayCV,"interAssayCV", id);
                                dataset.addValue(totalAssayCV,"totalCV", id);
                                                                          
                            }
                        }
                        break;
                }       
            }         
            return dataset;
        }
        
        /*
        private Set<String> getConcentrationLevels(HashMap<String, 
                ExpIIMiniValidationOfRepeatabilityResult> peptideRepeatabilityValidationResultMap) {
            //throw new UnsupportedOperationException("Not yet implemented");
            Set<String> concLevels = null;
            Set<String> peptideSequences = peptideRepeatabilityValidationResultMap.keySet();
            ExpIIMiniValidationOfRepeatabilityResult repeatabilityResult = 
                    peptideRepeatabilityValidationResultMap.get(peptideSequences.iterator().next()); 
                                //get the first element's mapping in set

            HashMap<String, ExpIIValidationIdAndConcLevelsCoefs>  subIdsToConcLevelsCoefsMap = 
                    repeatabilityResult.getSubIdsToConcLevelsCoefsMap();
            Set<String> subIds = subIdsToConcLevelsCoefsMap.keySet(); //transition Ids
            ExpIIValidationIdAndConcLevelsCoefs subIdAndConcLevelsCoefs = //first transitionId and its conc Levels coefficients
                    subIdsToConcLevelsCoefsMap.get(subIds.iterator().next());

            HashMap<String, ExpIIConcLevelCoefficientOfVariation> concLevelToCoeffficientsMap =
                    subIdAndConcLevelsCoefs.getConcLevelToCoeffficientsMap();

            concLevels = concLevelToCoeffficientsMap.keySet();
            return concLevels;
        }
        */       

        private HashMap<String, JFreeChart> generateResponsePeptideResultCharts(MRMPlusPlotDataCollection plotDataCollection) {
            HashMap<String, JFreeChart> peptideResponseResultCharts = new HashMap<String, JFreeChart>();
            HashMap<String, //peptide
                    HashMap<String, //transition 
                    MRMPlusTransitionXYSeries>> responsePlotData =  plotDataCollection.getResponseCurvePlotData();
            
            Set<String> peptides = responsePlotData.keySet();
            for(String peptide : peptides){
                // get peptide associated maps...
                HashMap<String, MRMPlusTransitionXYSeries> trXYSeriesMap = responsePlotData.get(peptide);
                // make XYSerieCollection Dataset from... 
                XYSeriesCollection peptideDataPlotSeriesDataset = new XYSeriesCollection( ); 
                Set<String> transitions = trXYSeriesMap.keySet();
                for(String transition : transitions){
                    XYSeries trPlotSeries = new XYSeries( transition ); 
                    MRMPlusTransitionXYSeries dataseries = trXYSeriesMap.get(transition);
                    LinkedList<MRMPlusPlotCoordinate> plotCoords = dataseries.getCoordinates();
                    for(MRMPlusPlotCoordinate plotCoord : plotCoords){
                        trPlotSeries.add(plotCoord.getXcoord(), plotCoord.getYcoord());
                    }
                    peptideDataPlotSeriesDataset.addSeries(trPlotSeries);
                }
                //make JFreeChart
                JFreeChart peptideChart = ChartFactory.createXYLineChart(
                                            "Response Curve" ,
                                            "Theoretical amount (pmol)" ,
                                            "Peak Area Ratio (Heavy/Light)" ,
                                            peptideDataPlotSeriesDataset ,
                                            PlotOrientation.VERTICAL ,
                                            true , true , false);
                peptideResponseResultCharts.put(peptide, peptideChart);
            }
            
            
            return peptideResponseResultCharts;
        }

        private HashMap<String, JFreeChart> 
            generateReapetabilityPeptideResultCharts(MRMPlusPlotDataCollection plotDataCollection) {
            HashMap<String, JFreeChart> peptideRepeatResultCharts = new HashMap<String, JFreeChart>();
            
            HashMap<String, //peptide String
                    HashMap<String, //transition String
                    HashMap<String, //Day(Time) String
                    HashMap<String, //concLevel [Hi, Me, Lo] String/id 
                    MRMPlusTransitionXYSeries>>>> repeatPlotData = plotDataCollection.getRepeatQCPlotData();
            
            Set<String> peptides = repeatPlotData.keySet();
            for(String peptide : peptides){
                
                // make XYSerieCollection Dataset from... 
                XYSeriesCollection peptideDataPlotSeriesDataset = new XYSeriesCollection( );            
                DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
                
                //drill down to the concLevel data...
                HashMap<String, //transition String
                    HashMap<String, //Day(Time) String
                    HashMap<String, //concLevel [Hi, Me, Lo] String/id 
                    MRMPlusTransitionXYSeries>>> trXYSeriesPlotData = repeatPlotData.get(peptide);
                
                // in this implementation only extract the SUMMED dataseries....
                HashMap<String, HashMap<String, MRMPlusTransitionXYSeries>> dayXYSeriesPlotData = 
                        trXYSeriesPlotData.get("SUMMED");
                Set<String> days = dayXYSeriesPlotData.keySet();
                
                System.out.println("\nFound these datapoints for peptide " + peptide);
                
                LinkedList<MRMPlusDefaultDataCoordinate> datasetCoords = new LinkedList<MRMPlusDefaultDataCoordinate>();
                for(String day : days){
                    //extract the info for conc levels....
                    HashMap<String, MRMPlusTransitionXYSeries> concLevelXYSeriesPlotData = 
                        dayXYSeriesPlotData.get(day);
                    Set<String> concLevels = concLevelXYSeriesPlotData.keySet();
                    
                    for(String concLevel : concLevels){
                        MRMPlusTransitionXYSeries xYSeries = concLevelXYSeriesPlotData.get(concLevel);
                        LinkedList<MRMPlusPlotCoordinate> xyCoords = xYSeries.getCoordinates();
                        // create a series based-off the concLevel and the day of assay...
                        //XYSeries dayConcLevelXYSeries = new XYSeries(concLevel + "_" + day);
                        double[] values = new double[xyCoords.size()];
                        for(int i = 0; i < values.length; i++){
                            MRMPlusPlotCoordinate xyCoord = xyCoords.get(i);
                            //double xCoord = xyCoord.getXcoord(); // Day
                            double value = xyCoord.getYcoord(); // value 
                            //dayConcLevelXYSeries.add(xyCoord.getXcoord(), xyCoord.getYcoord());
                            //int dayInteger = (int) xCoord;
                            //MRMPlusDefaultDataCoordinate dataCoord = 
                            //        new MRMPlusDefaultDataCoordinate(day ,value,concLevel);
                            //datasetCoords.add(dataCoord);
                            
                            values[i] = value;
                        }  
                        Median median = new Median();
                        double medianValue = median.evaluate(values);
                        //peptideDataPlotSeriesDataset.addSeries(dayConcLevelXYSeries);
                        //dataset.addValue( 60 , "schools" ,  "1990" );
                        dataset.addValue(medianValue, concLevel, day);
                        
                        // debugging...
                        System.out.println(day +"; " + concLevel +"; " + medianValue);
                    }
                }  
                
                
                //make JFreeChart
                JFreeChart peptideChart = ChartFactory.createLineChart(
                                            "Repeatability" ,
                                            "Time (Day)" ,
                                            "Measured (Peak Area Ratio)" ,
                                            //peptideDataPlotSeriesDataset ,
                                            dataset,
                                            PlotOrientation.VERTICAL ,
                                            true , true , false);
                peptideRepeatResultCharts.put(peptide, peptideChart);
            }
            
            return peptideRepeatResultCharts;
        }
    }
}
