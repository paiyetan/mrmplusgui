/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrmplus.gui;

import java.util.HashMap;
import org.jfree.chart.JFreeChart;

/**
 *
 * @author paiyeta1
 */
public class MRMPlusResultCharts {
    
    private HashMap<String, JFreeChart> generalResultCharts;
    private HashMap<String, JFreeChart> peptideResponseResultCharts;
    private HashMap<String, JFreeChart> peptideRepeatResultCharts;

    public MRMPlusResultCharts(HashMap<String, JFreeChart> generalCharts, 
                    HashMap<String, JFreeChart> peptideResponseCharts, 
                        HashMap<String, JFreeChart> peptideRepeatCharts) {
        this.generalResultCharts = generalCharts;
        this.peptideResponseResultCharts = peptideResponseCharts;
        this.peptideRepeatResultCharts = peptideRepeatCharts;
    }

    public HashMap<String, JFreeChart> getGeneralCharts() {
        return generalResultCharts;
    }

    public HashMap<String, JFreeChart> getPeptideResponseCharts() {
        return peptideResponseResultCharts;
    }

    public HashMap<String, JFreeChart> getPeptideRepeatCharts() {
        return peptideRepeatResultCharts;
    }

    public void setGeneralCharts(HashMap<String, JFreeChart> generalCharts) {
        this.generalResultCharts = generalCharts;
    }

    public void setPeptideResponseCharts(HashMap<String, JFreeChart> peptideResponseCharts) {
        this.peptideResponseResultCharts = peptideResponseCharts;
    }

    public void setPeptideRepeatCharts(HashMap<String, JFreeChart> peptideRepeatCharts) {
        this.peptideRepeatResultCharts = peptideRepeatCharts;
    }
    
    
}
