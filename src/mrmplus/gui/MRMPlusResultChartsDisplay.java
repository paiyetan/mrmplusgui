/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrmplus.gui;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;

/**
 *
 * @author paiyeta1
 */
public class MRMPlusResultChartsDisplay implements ActionListener {
    
    private final HashMap<String, JFreeChart> jfcs;
    private final HashMap<String, JFreeChart> peptideResponseJFCs;
    private final HashMap<String, JFreeChart> peptideRepeatJFCs;
    
    JFrame f;
    JTabbedPane jtp;
    JPanel pepSelectionPanel;
    JLabel peptideLabel;
    JComboBox peptideComboBox; 
    JLabel expLabel;
    JComboBox expComboBox;
    JButton viewButton;
    JPanel chartPanel;
    JPanel peptidesQCPanel; 
        
    
    public MRMPlusResultChartsDisplay(MRMPlusResultCharts resultCharts) {
        this.jfcs = resultCharts.getGeneralCharts();
        this.peptideResponseJFCs = resultCharts.getPeptideResponseCharts();
        this.peptideRepeatJFCs = resultCharts.getPeptideRepeatCharts();
    }

    public void display() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //JFrame f = new JFrame("MRMPlusGUI Result Charts...");
        f = new JFrame("MRMPlusGUI Result Charts...");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //final JTabbedPane jtp = new JTabbedPane();
        jtp = new JTabbedPane();
        
        // add general charts to the pane (tabbed pane)
        Set<String> tabPanelIds = jfcs.keySet();
        for(String tabPanelId : tabPanelIds){
            jtp.add(tabPanelId, new ChartPanel(jfcs.get(tabPanelId)));
        }
        
        //create a Panel to hold peptide specific results...
        //final JPanel peptidesQCPanel = new JPanel();
        peptidesQCPanel = new JPanel(new BorderLayout());
        
        //JPanel pepSelectionPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        //JLabel peptideLabel = new JLabel("Peptide: ");
        //final JComboBox peptideComboBox = new JComboBox();
        pepSelectionPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        peptideLabel = new JLabel("Peptide: ");
        peptideComboBox = new JComboBox();
        
        
        
        //populateComboBox items
        Set<String> peptides = peptideResponseJFCs.keySet();
        for(String peptide : peptides){
            peptideComboBox.addItem(peptide);
        }
        
        //JLabel expLabel = new JLabel("Experiment: ");
        //final JComboBox expComboBox = new JComboBox();
        expLabel = new JLabel("Experiment: ");
        expComboBox = new JComboBox();
        
        // populate experiment ComboBox
        expComboBox.addItem("Response");
        expComboBox.addItem("Repeatability");
        
        
        //JButton viewButton = new JButton("View");
        viewButton = new JButton("View");
        viewButton.addActionListener(this);
        
        pepSelectionPanel.add(peptideLabel);
        pepSelectionPanel.add(peptideComboBox);
        pepSelectionPanel.add(expLabel);
        pepSelectionPanel.add(expComboBox);
        pepSelectionPanel.add(viewButton);
        
        // add the selection panel to the north of the QC panel...
        peptidesQCPanel.add(pepSelectionPanel, BorderLayout.NORTH);
        
        // add the view panel(visualization pane) to the south/center of the QC panel...
        // ***** add script here *****
        //JPanel viewPanel = new JPanel();
        String selectedPeptide = (String) peptideComboBox.getSelectedItem();
        String selectedExp = (String) expComboBox.getSelectedItem();
        //String selectedExp = "Repeatability"; // testing on Response Charts...
        
        JFreeChart chartToView;
        if(selectedExp.equalsIgnoreCase("Response")){
            chartToView = peptideResponseJFCs.get(selectedPeptide);            
        }else{
            chartToView = peptideRepeatJFCs.get(selectedPeptide);
        }
        chartPanel = new ChartPanel(chartToView);
        peptidesQCPanel.add(chartPanel, BorderLayout.SOUTH);
        
        /*
        //add action listener to button
        viewButton.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //get selected peptide
                //get selected experiment..
                String selectedPeptide = (String) peptideComboBox.getSelectedItem();
                String selectedExp = (String) expComboBox.getSelectedItem();
                //get appropriate JFreeChart...
                //redraw the panel 
                JFreeChart chartToView;
                if(selectedExp.equalsIgnoreCase("Response")){
                    chartToView = peptideResponseJFCs.get(selectedPeptide);            
                }else{
                    chartToView = peptideRepeatJFCs.get(selectedPeptide);
                }
                ChartPanel viewChartPanel = new ChartPanel(chartToView);
                peptidesQCPanel.repaint();
                peptidesQCPanel.add(viewChartPanel, BorderLayout.SOUTH);

            }
        });
        */
        
        
        // add peptide QC Panel to the tabbed panel...
        jtp.add("Peptides", peptidesQCPanel);
        // add JTabbedPane to the Frame...   
        f.add(jtp, BorderLayout.CENTER);
        /*
        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        p.add(new JButton(new AbstractAction("Add") {
            @Override
            public void actionPerformed(ActionEvent e) {
                jtp.add(String.valueOf(++n), createPane());
                jtp.setSelectedIndex(n - 1);
            }
        }));
        f.add(p, BorderLayout.SOUTH);
        */
        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
      
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
                //numClicks++;
                //text.setText("Button Clicked " + numClicks + " times");
        String selectedPeptide = (String) peptideComboBox.getSelectedItem();
        String selectedExp = (String) expComboBox.getSelectedItem();
        //get appropriate JFreeChart...
        //redraw the panel 
        JFreeChart chartToView;
        if(selectedExp.equalsIgnoreCase("Response")){
            chartToView = peptideResponseJFCs.get(selectedPeptide);            
        }else{
            chartToView = peptideRepeatJFCs.get(selectedPeptide);
            // edit plot attributes....
            //Plot plot = chartToView.getPlot();
            //XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer( );
            //renderer.setSeriesPaint( 0 , Color.RED );
            //renderer.setSeriesPaint( 1 , Color.GREEN );
            //renderer.setSeriesPaint( 2 , Color.YELLOW );
            //renderer.setSeriesStroke( 0 , new BasicStroke( 4.0f ) );
            //renderer.setSeriesStroke( 1 , new BasicStroke( 3.0f ) );
            //renderer.setSeriesStroke( 2 , new BasicStroke( 2.0f ) );
            
            //renderer.setSeriesShapesVisible(0, true );
            //renderer.setSeriesShapesVisible( 1 , true );
            //renderer.setSeriesShapesVisible( 2 , true );
            
            //plot..setRenderer( renderer ); 
            //chartToView = new JFreeChart(plot);
            //chartToView.
        }
        JPanel viewChartPanel = new ChartPanel(chartToView);
        //peptidesQCPanel.repaint();
        peptidesQCPanel.removeAll();
        
        // add the selection panel to the north of the QC panel...
        peptidesQCPanel.add(pepSelectionPanel, BorderLayout.NORTH);
        peptidesQCPanel.add(viewChartPanel, BorderLayout.SOUTH);
        peptidesQCPanel.validate();
        
        
    }
}
