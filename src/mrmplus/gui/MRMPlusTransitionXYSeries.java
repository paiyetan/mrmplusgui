/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrmplus.gui;

import java.util.LinkedList;

/**
 *
 * @author paiyeta1
 */
public class MRMPlusTransitionXYSeries {
    
    private String id;
    private LinkedList<MRMPlusPlotCoordinate> coordinates;

    public MRMPlusTransitionXYSeries(){
        coordinates = new LinkedList<MRMPlusPlotCoordinate>();
    }
    
    public MRMPlusTransitionXYSeries(String id) {
        this.id = id;
        coordinates = new LinkedList<MRMPlusPlotCoordinate>();
    }
    
    public MRMPlusTransitionXYSeries(String id, LinkedList<MRMPlusPlotCoordinate> coordinates) {
        this.id = id;
        this.coordinates = coordinates;
    }
    
    
    public void addCoordinate(MRMPlusPlotCoordinate xYCoord){
        coordinates.add(xYCoord);
    }
    
    public void addCoordinate(double x, double y) {
        // throw new UnsupportedOperationException("Not supported yet."); 
        // To change body of generated methods, choose Tools | Templates.
        addCoordinate(new MRMPlusPlotCoordinate(x, y));
    }
     

    public String getId() {
        return id;
    }

    public LinkedList<MRMPlusPlotCoordinate> getCoordinates() {
        return coordinates;
    }

      
    
}
