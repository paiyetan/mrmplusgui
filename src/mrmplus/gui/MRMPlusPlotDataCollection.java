/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrmplus.gui;

import java.util.HashMap;

/**
 *
 * @author paiyeta1
 */
public class MRMPlusPlotDataCollection {                                                //[]
                                                           //[transitionIdString, LinkedList<PlotCoordinate>]] 
                    //<peptideString, HashMap<transitionIdString, MRMPlusTransitionXYSeries>> 
    private HashMap<String, HashMap<String, MRMPlusTransitionXYSeries>> responseCurvePlotData;
    private HashMap<String, //peptide String
                    HashMap<String, //transition String
                            HashMap<String, //Day(Time) String
                                    HashMap<String, //concLevel [Hi, Me, Lo] String/id 
                                            MRMPlusTransitionXYSeries>>>> repeatQCPlotData;
                                                //[id(concLevel) string,LinkedList<PlotCoordinate>]

    public MRMPlusPlotDataCollection() {
        // instantiate inner objects....
        //MRMPlusTransitionXYSeries initTResponseSeries = new MRMPlusTransitionXYSeries();
        
        responseCurvePlotData = new HashMap<String, 
                                        HashMap<String, 
                                                MRMPlusTransitionXYSeries>>();
        repeatQCPlotData = new HashMap<String, //peptide String
                                    HashMap<String, //transition String
                                            HashMap<String, //Day(Time) String
                                                    HashMap<String, //concLevel [Hi, Me, Lo] String/id 
                                                            MRMPlusTransitionXYSeries>>>>();
        
    }

    public HashMap<String, HashMap<String, 
                    MRMPlusTransitionXYSeries>> getResponseCurvePlotData() {
        return responseCurvePlotData;
    }

    public HashMap<String, 
                    HashMap<String, 
                        HashMap<String, 
                            HashMap<String, MRMPlusTransitionXYSeries>>>> getRepeatQCPlotData() {
        return repeatQCPlotData;
    }   

    public void setResponseCurvePlotData(HashMap<String, 
            HashMap<String, MRMPlusTransitionXYSeries>> responseCurvePlotData) {
        this.responseCurvePlotData = responseCurvePlotData;
    }

    public void setRepeatQCPlotData(HashMap<String, 
            HashMap<String, HashMap<String, 
                HashMap<String, MRMPlusTransitionXYSeries>>>> repeatQCPlotData) {
        this.repeatQCPlotData = repeatQCPlotData;
    }
    
                            
                            
}
