/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrmplus.gui;

/**
 *
 * @author paiyeta1
 */
public class MRMPlusPlotCoordinate {
    
    private double xcoord;
    private double ycoord;
    
    public MRMPlusPlotCoordinate(double x, double y){
        this.xcoord = x;
        this.ycoord = y;
    }

    public double getXcoord() {
        return xcoord;
    }

    public double getYcoord() {
        return ycoord;
    }
    
    
    
    
}
