/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrmplus.gui;

/**
 *
 * @author paiyeta1
 */
public class MRMPlusDefaultDataCoordinate {
    
    private String x; // or any x value variable
    private double y; // associated value....
    private String type; // the type for which the given variables are associated...

    public MRMPlusDefaultDataCoordinate(String x, double y, String type) {
        this.x = x;
        this.y = y;
        this.type = type;
    }

    public String getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public String getType() {
        return type;
    }
    
    
    
}
